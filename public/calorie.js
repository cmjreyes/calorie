search_url=" http://api.data.gov/usda/ndb/search/?format=JSON";
info_url=" http://api.data.gov/usda/ndb/reports/?format=JSON";
key="oNXdtdXKDN5XP1p12fl5EFKTrDTtvcquSpFwDJXc";

angular.module("calorie_count", ['ui.bootstrap'])
	.controller("CalorieController", function($scope, $http) {

		$scope.calorie = {query:"", dayCount: 7, items: [], selected_items: []};
		$scope.nutrition = {calories: 0, carbs: 0, protein: 0, fat: 0};

		$scope.query = function(val) {

			return $http.get(search_url, {
				params: {
					q: val,
					api_key: key
				}
			})
			.then(function(data){
				return data.data.list.item;
			});			
		}

		$scope.addToList = function(item, model, label) {
			// we do a quick add of the information we have, then
			// spin a request off for more info, which'll be populated in a bit
			// note we mirror the structure of the response we expect back to
			// make updating the model easier			
			$scope.calorie.selected_items.unshift({
					food: { name: item.name },
					count: 0,
					measure: "",
					nutrients: {
						energy: {
							unit: "",
							measures: []
						},
						carbs: {
							unit: "",
							measures: []
						},
						protein: {
							unit: "",
							measures: []
						},
						fat: {
							unit: "",
							measures: []
						}
					}
				});

			request = "&ndbno=" + item.ndbno + "&api_key=" + key;
			$http.get(info_url+request)
				.success(function(data, status, headers, config){

					// populate with the data we care about, basically
					// the 3 macros and their measures
					nutrients = data.report.food.nutrients;

					for(var i = 0; i < nutrients.length; i++) {
						nutrient = nutrients[i];

						switch(nutrient.name) {
							case "Energy":
								$scope.calorie.selected_items[0].nutrients.energy.measures = nutrient.measures;
								$scope.calorie.selected_items[0].nutrients.energy.unit = nutrient.unit;
							break;
							case "Protein":
								$scope.calorie.selected_items[0].nutrients.protein.measures = nutrient.measures;
								$scope.calorie.selected_items[0].nutrients.protein.unit = nutrient.unit;
							break;
							case "Carbohydrate, by difference":
								$scope.calorie.selected_items[0].nutrients.carbs.measures = nutrient.measures;
								$scope.calorie.selected_items[0].nutrients.carbs.unit = nutrient.unit;
							break;
							case "Total lipid (fat)":
								$scope.calorie.selected_items[0].nutrients.fat.measures = nutrient.measures;
								$scope.calorie.selected_items[0].nutrients.fat.unit = nutrient.unit;
							break;
						}
					}
				});	
		}

		$scope.removeFromList = function(item) {
			var index = $scope.calorie.selected_items.indexOf(item);
			$scope.calorie.selected_items.splice(index, 1);

			// we need to recalc nutrition because we've removed something
			$scope.updateNutrition();
		}

		/**
		 * I DONT KNOW WHAT IT DOES YET @TODO @YOLO
		 *
		 * @param item the item who's change has envoked this change
		 */
		$scope.updateNutrition = function() {
			var energy = 0;
			var fat = 0;
			var carbs = 0;
			var protein = 0;
			var items = $scope.calorie.selected_items;

			// thinking out loud. The values in the ^ item are already set.
			// So I just need to recalc - all the info's there.
			for(var i = 0; i < items.length; i++) {
				item = items[i];
				// As far as total calc goes, we need to 0 out at this point
				// and do a full recalc - which would let us 

				// for each macro thing we need to get the value for the correct measure
				// let's hope all the measures are consistent across nutrients
				measure_index = item.nutrients.energy.measures.indexOf(item.measure);

				if(measure_index >= 0) {
					// calories
					measure_obj = item.nutrients.energy.measures[measure_index];
					energy += (measure_obj.value) * item.count;				

					// protein
					measure_obj = item.nutrients.protein.measures[measure_index];
					protein += (measure_obj.value) * item.count;				

					// fats
					measure_obj = item.nutrients.fat.measures[measure_index];
					fat += (measure_obj.value) * item.count;				

					// carbs
					measure_obj = item.nutrients.carbs.measures[measure_index];
					carbs += (measure_obj.value) * item.count;				
				}
			}

			$scope.nutrition.calories = energy;
			$scope.nutrition.protein = protein;
			$scope.nutrition.fat = fat;
			$scope.nutrition.carbs = carbs;
		}
	});
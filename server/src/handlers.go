package main

import (
	"fmt"
	"net/http"
	"html"
	"encoding/json"
)

func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))
}

func Search(w http.ResponseWriter, r *http.Request) {
	products := Products{
		Product{Name: "Apple"},
		Product{Name: "Tuna"},
	}

	w.Header().Set("Content-Type", "application/json;charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(products); err != nil {
		panic(err)
	}
}

func Info(w http.ResponseWriter, r *http.Request) {
	
	json.NewEncoder(w).Encode(Product{Name: "Tuna"})
}
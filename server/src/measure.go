package main

type Measure struct {
	Label string `json:"label"`
	Grams int `json:"grams"`
}

type Measures []Measure
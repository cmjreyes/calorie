package main 

import "fmt"

var currentId int
var products Products

// seed data
func init() {
	RepoCreateProduct(Product{Name: "Apple"})
	RepoCreateProduct(Product{Name: "Tuna"})
}

func RepoFindTodo(id int) Product {
	for _, p := range products {
		if p.Id == id {
			return p
		}
	}

	// return empty Product?
	return Product{}
}

func RepoCreateProduct(p Product) Product {
	currentId += 1
	p.Id = currentId
	products = append(products, p)
	return p
}

func RepoDestroyProduct(id int) error {
	for i, p := range products {
		if p.Id == id {
			products = append(products[:i], products[i+1]...)
			return nil
		}
	}
	return fmt.Errorf("could not find Product with id of %d to delete", id)
}
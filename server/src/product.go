package main

type Product struct {
	Id int `json:"id"`
	measures Measures `json:"measures"`
	Name string `json:"name"`
	Calories int `json:"calories"`
	Fat int `json:"fat"`
	Protein int `json:"protein"`
	Carbs int `json:"carbs"`
}

type Products []Product